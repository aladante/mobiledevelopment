package com.hva.geoswipe.pojo;

import com.hva.geoswipe.R;

public class GeoItem {

    private int filename;
    private boolean europe;
    private String naam;

    public GeoItem(int filename, boolean europe, String naam) {
        this.filename = filename;
        this.europe = europe;
        this.naam = naam;
    }

    public int getFilename() {
        return filename;
    }

    public void setFilename(int filename) {
        this.filename = filename;
    }

    public boolean isEurope() {
        return europe;
    }

    public void setEurope(boolean europe) {
        this.europe = europe;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public static final int[] files = {
            R.drawable.img1_yes_denmark,
            R.drawable.img2_no_canada,
            R.drawable.img3_no_bangladesh,
            R.drawable.img4_yes_kazachstan,
            R.drawable.img5_no_colombia,
            R.drawable.img6_yes_poland,
            R.drawable.img7_yes_malta,
            R.drawable.img8_no_thailand
    };

    public static final boolean[] jaNee = {
            true,
            false,
            false,
            true,
            false,
            true,
            true,
            false
    };
}

