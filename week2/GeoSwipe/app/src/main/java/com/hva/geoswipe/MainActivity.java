package com.hva.geoswipe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hva.geoswipe.pojo.GeoItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button mRestart;
    private TextView mScore;
    private ImageView mImage;
    private TextView mNaam;

    private List<GeoItem> images;

    // to check where the thing left
    private int index;

    //coordinates to check swipe location
    private float x1, x2;
    private float y1, y2;

    // answer to the last anwer before the index is incremented
    private boolean answer;

    // base score is 0
    private int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRestart = findViewById(R.id.button);
        mScore = findViewById(R.id.score);
        mImage = findViewById(R.id.image);
        mNaam = findViewById(R.id.naam);
        images = getImageList();

        // to randomize the images
        shuffleList();
        getImage();

    }

    public void onClickBtn(View v) {
        reset();
        Toast.makeText(this, "Clicked on Button", Toast.LENGTH_LONG).show();
    }

    public void onClickImage(View v) {
        Toast.makeText(this, "Dit is land " + images.get(index).getNaam() , Toast.LENGTH_LONG).show();
    }

    public boolean onTouchEvent(MotionEvent touchevent) {
        switch (touchevent.getAction()) {
            // when user first touches the screen we get x and y coordinate
            case MotionEvent.ACTION_DOWN: {
                x1 = touchevent.getX();
                y1 = touchevent.getY();
                break;
            }
            case MotionEvent.ACTION_UP: {
                x2 = touchevent.getX();
                y2 = touchevent.getY();
                final boolean inEurope = true;
                final boolean notInEurope = false;

                //if left to right sweep event on screen this is good excelent
                if (x1 < x2) {
                    checkInput(inEurope);
                    Toast.makeText(this, "Left to Right Swap Performed", Toast.LENGTH_LONG).show();
                }

                // if right to left sweep event on screen
                //this is a bad not in europe
                if (x1 > x2) {
                    checkInput(notInEurope);
                    Toast.makeText(this, "Right to Left Swap Performed", Toast.LENGTH_LONG).show();
                }

                // if UP to Down sweep event on screen
                if (y1 < y2) {
                    Toast.makeText(this, "UP to Down Swap Performed", Toast.LENGTH_LONG).show();
                }

                //if Down to UP sweep event on screen
                if (y1 > y2) {
                    Toast.makeText(this, "Down to UP Swap Performed", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
        return false;
    }

    private List<GeoItem> getImageList() {
        final int[] files = {
                R.drawable.img1_yes_denmark,
                R.drawable.img2_no_canada,
                R.drawable.img3_no_bangladesh,
                R.drawable.img4_yes_kazachstan,
                R.drawable.img5_no_colombia,
                R.drawable.img6_yes_poland,
                R.drawable.img7_yes_malta,
                R.drawable.img8_no_thailand
        };

        final String[] naam = {
                "denmark",
                "canada",
                "bangladesh",
                "kazachstan",
                "colombia",
                "poland",
                "malta",
                "thailand"
        };

        final boolean[] jaNee = {
                true,
                false,
                false,
                true,
                false,
                true,
                true,
                false
        };

        List<GeoItem> images = new ArrayList<>();

        for (int i = 0; i < files.length; i++) {
            GeoItem item = new GeoItem(files[i], jaNee[i], naam[i]);
            images.add(item);
        }
        index = images.size() - 1;
        return images;
    }

    private void getImage() {
        if (index < 0) {
            reset();
        } else {
            answer = images.get(index).isEurope();
            mImage.setImageResource(images.get(index--).getFilename());
        }
    }

    private void checkInput(boolean input) {
        if (input == answer) {
            mScore.setText(String.valueOf(++score));
            getImage();
        } else {
            getImage();
            // min score
        }
    }

    private void shuffleList() {
        Collections.shuffle(images);
    }

    private void reset() {
        images = getImageList();
        score = 0;
        mScore.setText(String.valueOf(score));
        shuffleList();
    }
}
