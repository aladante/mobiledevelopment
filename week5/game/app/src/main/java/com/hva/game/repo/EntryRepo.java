package com.hva.game.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import com.hva.game.dao.EntryRoomDatabase;
import com.hva.game.dao.EntryDao;
import com.hva.game.pojo.Entry;

import java.util.List;

public class EntryRepo {

    private EntryDao entryDao;
    private LiveData<List<Entry>> allEntries;
    private EntryRoomDatabase db;

    public EntryRepo(Application application) {
        db = EntryRoomDatabase.getDatabase(application);
        entryDao = db.entryDao();
        allEntries = entryDao.getAllEntries();

    }

    public void insert(Entry entry) {
        new insertEntryAsyncTask(entryDao).execute(entry);
    }

    public void update (Entry entry) {
        new updateEntryAsyncTask(entryDao).execute(entry);
    }

    public void delete (Entry entry) {
        new deleteEntryAsyncTask(entryDao).execute(entry);
    }

    public void deleteAll() {
        new deleteAllEntryAsyncTask(entryDao).execute();
    }

    public LiveData<List<Entry>> getAllEntries() {
        return allEntries;
    }

    private static class insertEntryAsyncTask extends AsyncTask<Entry, Void, Void> {
        private EntryDao entryDao;

        private insertEntryAsyncTask(EntryDao entryDao) {
            this.entryDao = entryDao;
        }

        @Override
        protected Void doInBackground(Entry... entries) {
            entryDao.insert(entries[0]);
            return null;
        }
    }

    private static class updateEntryAsyncTask extends AsyncTask<Entry, Void, Void> {
        private EntryDao entryDao;

        private updateEntryAsyncTask(EntryDao entryDao) {
            this.entryDao = entryDao;
        }

        @Override
        protected Void doInBackground(Entry... entries) {
            entryDao.update(entries[0]);
            return null;
        }
    }

    private static class deleteEntryAsyncTask extends AsyncTask<Entry, Void, Void> {
        private EntryDao entryDao;

        private deleteEntryAsyncTask(EntryDao entryDao) {
            this.entryDao = entryDao;
        }

        @Override
        protected Void doInBackground(Entry... entries) {
            entryDao.delete(entries[0]);
            return null;
        }
    }

    private static class deleteAllEntryAsyncTask extends AsyncTask<Void, Void, Void> {
        private EntryDao entryDao;

        private deleteAllEntryAsyncTask(EntryDao entryDao) {
            this.entryDao = entryDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            entryDao.deleteAll();
            return null;
        }
    }
}
