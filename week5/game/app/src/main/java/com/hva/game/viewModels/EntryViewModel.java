package com.hva.game.viewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import com.hva.game.pojo.Entry;
import com.hva.game.repo.EntryRepo;

import java.util.List;

public class EntryViewModel extends AndroidViewModel {

    private EntryRepo entryRepo;
    private LiveData<List<Entry>> allEntries;

    public EntryViewModel(@NonNull Application application) {
        super(application);
        entryRepo = new EntryRepo(application);
        allEntries = entryRepo.getAllEntries();
    }

    public void instert(Entry entry) {
        entryRepo.insert(entry);
    }

    public void update(Entry entry) {
        entryRepo.update(entry);
    }

    public void delete(Entry entry) {
        entryRepo.delete(entry);
    }

    public void deleteAll() {
        entryRepo.deleteAll();
    }

    public LiveData<List<Entry>> getAllEntries() {
        return entryRepo.getAllEntries();
    }

}
