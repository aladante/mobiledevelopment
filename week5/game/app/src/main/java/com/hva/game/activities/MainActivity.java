package com.hva.game.activities;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.*;
import com.hva.game.MainAdapter;
import com.hva.game.R;
import com.hva.game.pojo.Entry;
import com.hva.game.viewModels.EntryViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    // de adapter die voor de recycler view zorg
    private ArrayList<Entry> entries = new ArrayList<>();
    private MainAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutmanager;

    //button op de main scherm/activity
    private Button mDelete;
    private FloatingActionButton mAdd;

    // dialoog voor een popup window om iets toe te voegen
    private Dialog myDialog;

    // load view model
    private EntryViewModel entryViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        buildAdapter();
        initButtons();

        // to laze to place clean this up
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                Toast.makeText(MainActivity.this, "deleted", Toast.LENGTH_SHORT).show();
                entryViewModel.delete(mAdapter.getEntrieAt(viewHolder.getAdapterPosition()));
            }
        }).attachToRecyclerView(mRecyclerView);
    }


    private void initButtons() {
        mAdd = findViewById(R.id.fab);
        mDelete = findViewById(R.id.delete);

        mAdd.setOnClickListener(v -> showPopup());

        mDelete.setOnClickListener(v -> entryViewModel.deleteAll());
    }

    private void buildAdapter() {
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutmanager = new LinearLayoutManager(this);
        mAdapter = new MainAdapter(entries);

        mRecyclerView.setLayoutManager(mLayoutmanager);
        mRecyclerView.setAdapter(mAdapter);

        entryViewModel = ViewModelProviders.of(this).get(EntryViewModel.class);
        // is gonna replace fill entries
        entryViewModel.getAllEntries().observe(this, new Observer<List<Entry>>() {
            @Override
            public void onChanged(@Nullable List<Entry> entries) {
                mAdapter.setEnrties(entries);
            }
        });

        mAdapter.OnItemClickListener(posistion -> {
            showPopupUpdate(posistion);
        });
    }


    public void showPopupUpdate(int posistion) {
        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.popup);

        TextView txtclose;
        Entry entry = mAdapter.getEntrieAt(posistion);
        Button btnAdd;
        Spinner mDropdown;


        txtclose = myDialog.findViewById(R.id.txtClose);
        btnAdd = myDialog.findViewById(R.id.buttonAdd);

        //fill drop down with content
        mDropdown = myDialog.findViewById(R.id.dropdown);
        EditText mTitle = myDialog.findViewById(R.id.txttitle);
        EditText mDesc = myDialog.findViewById(R.id.txtdesc);

        mTitle.setText(entry.getTitle());
        mDesc.setText(entry.getDesc());

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDropdown.setAdapter(adapter);
        mDropdown.setOnItemSelectedListener(this);

        mDropdown.setSelection(getIndex(mDropdown, entry.getStatus()));


        txtclose.setOnClickListener(v12 -> myDialog.dismiss());

        btnAdd.setOnClickListener(v1 -> {
            entry.update(mTitle.getText().toString(), mDesc.getText().toString(), mDropdown.getSelectedItem().toString());

            entryViewModel.update(entry);
            myDialog.dismiss();
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    //compare input spinner to make spinner same as selected
    private int getIndex(Spinner spinner, String myString) {

        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).equals(myString)) {
                index = i;
            }
        }
        return index;
    }

    // Popup to provide a screen to add new entries
    public void showPopup() {
        myDialog = new Dialog(this);
        TextView txtclose;

        Button btnAdd;
        Spinner mDropdown;

        myDialog.setContentView(R.layout.popup);
        txtclose = myDialog.findViewById(R.id.txtClose);
        btnAdd = myDialog.findViewById(R.id.buttonAdd);

        //fill drop down with content
        mDropdown = myDialog.findViewById(R.id.dropdown);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.items, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDropdown.setAdapter(adapter);
        mDropdown.setOnItemSelectedListener(this);
        txtclose.setOnClickListener(v12 -> myDialog.dismiss());
        btnAdd.setOnClickListener(v1 -> {
            EditText mTitle = myDialog.findViewById(R.id.txttitle);
            EditText mDesc = myDialog.findViewById(R.id.txtdesc);

            Entry newEntry = new Entry(mTitle.getText().toString(), mDropdown.getSelectedItem().toString(), mDesc.getText().toString());
            entryViewModel.instert(newEntry);
            myDialog.dismiss();
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    private void updateUI(List<Entry> entryList) {
        entries.clear();
        entries.addAll(entryList);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

