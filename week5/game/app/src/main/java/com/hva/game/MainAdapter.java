package com.hva.game;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hva.game.pojo.Entry;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.myViewHolder> {

    private List<Entry> mEnrties;
    private EntryClickListener mListener;

    public MainAdapter(ArrayList<Entry> entries) {
        mEnrties = entries;
    }


    public void OnItemClickListener(EntryClickListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        myViewHolder mvh = new myViewHolder(v, mListener, mEnrties);
        return mvh;
    }

    public static class myViewHolder extends RecyclerView.ViewHolder{

        public TextView mConsole;
        public TextView mTitle;
        public TextView mStatus;
        public TextView mDate;


        public myViewHolder(@NonNull View itemView, final EntryClickListener listener, final List<Entry> entries) {
            super(itemView);

            mTitle = itemView.findViewById(R.id.title);
            mConsole = itemView.findViewById(R.id.console);
            mStatus = itemView.findViewById(R.id.state);
            mDate = itemView.findViewById(R.id.date);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public void onBindViewHolder(@NonNull myViewHolder myViewHolder, int i) {
        Entry entry = mEnrties.get(i);

        myViewHolder.mTitle.setText(entry.getTitle());
        myViewHolder.mConsole.setText(entry.getDesc());
        myViewHolder.mStatus.setText(entry.getStatus());
        myViewHolder.mDate.setText(entry.getDate());
    }

    public void setEnrties(List<Entry> enrties) {
        this.mEnrties = enrties;
        notifyDataSetChanged();
    }

    public Entry getEntrieAt(int posistion) {
        return mEnrties.get(posistion);
    }

    @Override
    public int getItemCount() {
        return mEnrties.size();
    }
}


