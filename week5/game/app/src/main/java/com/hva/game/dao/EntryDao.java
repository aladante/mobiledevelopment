package com.hva.game.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;
import com.hva.game.pojo.Entry;

import java.util.List;

@Dao
public interface EntryDao {

    @Insert
    void insert(Entry entry);

    @Update
    void update(Entry entry);

    @Delete
    void delete(Entry entry);

    @Query("DELETE FROM entry")
    void deleteAll();

    @Query("SELECT * from entry")
    LiveData<List<Entry>> getAllEntries();
}

