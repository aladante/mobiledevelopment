package com.hva.bucketlist.doa;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.hva.bucketlist.pojo.Entry;

import java.util.List;

@Dao

public interface EntryDao {

	@Insert
	void insert(Entry entry);

	@Delete
	void delete(Entry entry);

	@Delete
	void delete(List<Entry> entryList);

	@Query("SELECT * from entry")
	List<Entry> getAllEntries();
}
