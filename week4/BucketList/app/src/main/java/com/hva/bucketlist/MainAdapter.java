package com.hva.bucketlist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import com.hva.bucketlist.pojo.Entry;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.myViewHolder> {

    private ArrayList<Entry> mEnrties;
    private EntryClickListener mListener;

    public MainAdapter(ArrayList<Entry> entries) {
        mEnrties = entries;
    }


    public void OnItemClickListener(EntryClickListener listener) {
        mListener = listener;
    }

    public static class myViewHolder extends RecyclerView.ViewHolder{

        public TextView mTitle;
        public TextView mDesc;
        public RadioButton mSelected;

        public myViewHolder(@NonNull View itemView, final EntryClickListener listener, final List<Entry> entries) {
            super(itemView);

            mSelected = itemView.findViewById(R.id.radioButton);
            mTitle = itemView.findViewById(R.id.title);
            mDesc = itemView.findViewById(R.id.desc);

            mSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            Entry entry = entries.get(position);
                            listener.onRadioClick(entry);
                            mSelected.setChecked(entry.isSelected());
                        }
                    }
                }
            });
//            only if i want to make whole item click able don't see the need yet

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (listener != null) {
//                        int position = getAdapterPosition();
//                        if (position != RecyclerView.NO_POSITION) {
//                            listener.onItemClick(position);
//                        }
//                    }
//                }
//            });
        }
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        myViewHolder mvh = new myViewHolder(v, mListener, mEnrties);
        return mvh;
    }

    public void onBindViewHolder(@NonNull myViewHolder myViewHolder, int i) {
        Entry entry = mEnrties.get(i);
        myViewHolder.mTitle.setText(entry.getTitle());
        myViewHolder.mDesc.setText(entry.getDesc());
    }

    @Override
    public int getItemCount() {
        return mEnrties.size();
    }
}

