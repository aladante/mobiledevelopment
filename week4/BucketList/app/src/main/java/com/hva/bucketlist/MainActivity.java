package com.hva.bucketlist;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.hva.bucketlist.pojo.Entry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    // de adapter die voor de recycler view zorg
    private ArrayList<Entry> entries = new ArrayList<>();
    private MainAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutmanager;

    //button op de main scherm/activity
    private Button mDelete;
    private FloatingActionButton mAdd;

    // dialoog voor een popup window om iets toe te voegen
    private Dialog myDialog;

    // database
    private EntryRoomDatabase db;
    // the database uses volitalo keyword and is multithreading for this we need to execute threads to make it presistent
    private Executor executor = Executors.newSingleThreadExecutor();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = EntryRoomDatabase.getDatabase(this);

        // is gonna replace fill entries
        getAllProducts();
        buildAdapter();
        initButtons();
    }

    private void initButtons() {
        mAdd = findViewById(R.id.fab);
        mDelete = findViewById(R.id.delete);

        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });

        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAllSelectedEntries();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void buildAdapter() {
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutmanager = new LinearLayoutManager(this);
        mAdapter = new MainAdapter(entries);

        mRecyclerView.setLayoutManager(mLayoutmanager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.OnItemClickListener(new EntryClickListener() {
            @Override
            public void onRadioClick(Entry entry) {
                entry.toggle();
            }
        });
    }

    // Popup to provide a screen to add new entries
    public void showPopup(View v) {
        myDialog = new Dialog(this);
        TextView txtclose;
        Button btnAdd;

        myDialog.setContentView(R.layout.popup);
        txtclose = myDialog.findViewById(R.id.txtClose);
        btnAdd = myDialog.findViewById(R.id.buttonAdd);

        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText mTitle = myDialog.findViewById(R.id.txttitle);
                EditText mDesc = myDialog.findViewById(R.id.txtdesc);
                Entry newEntry = new Entry(mTitle.getText().toString(), mDesc.getText().toString());
                insertEntry(newEntry);
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void fillEntries() {
        entries.add(new Entry("test", "uno"));
        entries.add(new Entry("test", "dos"));
        entries.add(new Entry("test", "tres"));
    }

    private void deleteAllSelectedEntries() {
        ArrayList<Entry> deleteEntries = new ArrayList<>();
        for (Entry entry : entries) {
            if (entry.isSelected()) {
                deleteEntries.add(entry);
            }
        }
        deleteAllProducts(deleteEntries);
    }

    private void getAllProducts() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                final List<Entry> entries = db.entryDao().getAllEntries();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI(entries);
                    }
                });
            }
        });
    }

    private void insertEntry(final Entry entry) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.entryDao().insert(entry);
                getAllProducts();
            }
        });
    }

    private void deleteAllProducts(final List<Entry> products) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                db.entryDao().delete(products);
                getAllProducts();
            }
        });
    }

    private void updateUI(List<Entry> entryList) {
        entries.clear();
        entries.addAll(entryList);
        for(Entry entry : entries) {
            entry.setSelected(false);
        }
        mAdapter.notifyDataSetChanged();
    }
}
