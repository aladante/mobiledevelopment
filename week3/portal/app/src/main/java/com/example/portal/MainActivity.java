package com.example.portal;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.example.portal.pojo.Entry;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutmanager;
    private Button mBack;
    private Button mAdd;

    private Dialog myDialog;

    private ArrayList<Entry> entries = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createEntry();
        buildAdapter();

        myDialog = new Dialog(this);
    }

    public void ShowPopup(View v) {
        TextView txtclose;
        Button btnAdd;
        myDialog.setContentView(R.layout.pop_up_window);
        txtclose = myDialog.findViewById(R.id.txtClose);
        txtclose.setText("M");
        btnAdd = myDialog.findViewById(R.id.buttonAdd);

        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText mNaam = myDialog.findViewById(R.id.name);
                EditText mLink = myDialog.findViewById(R.id.link);
                Entry newEntry = new Entry(mNaam.getText().toString(), mLink.getText().toString());
                entries.add(newEntry);
                mAdapter.notifyDataSetChanged();
                myDialog.dismiss();
            }
        });

        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }


    private void buildAdapter() {
        mRecyclerView = findViewById(R.id.recyclerv_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutmanager = new LinearLayoutManager(this);
        mAdapter = new Adapter(entries);

        mRecyclerView.setLayoutManager(mLayoutmanager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.OnItemClickListener(new Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                // open web view with selected thingy
//                Intent i = new Intent(MainActivity.this,WebViewActivity.class);
//                startActivity(i);
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(URLUtil.guessUrl(entries.get(position).getLink())));
                startActivity(i);
            }
        });
    }

    private void createEntry() {
        entries.add(new Entry("https://www.google.com", "google"));
        entries.add(new Entry("https://www.skrt.com", "skrrt"));
        entries.add(new Entry("final", "test3"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
