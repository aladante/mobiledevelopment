package com.example.portal;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.portal.pojo.Entry;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends  RecyclerView.Adapter<Adapter.myViewHolder>  {

    private List<Entry> mEnrties;
    private OnItemClickListener mListener;

    public Adapter(ArrayList<Entry> entries) {
        mEnrties = entries;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void OnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public static class myViewHolder extends RecyclerView.ViewHolder{

        public TextView mLink;
        public TextView mNaam;

        public myViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            mLink = itemView.findViewById(R.id.htmlLink);
            mNaam = itemView.findViewById(R.id.normalName);
            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

    }


    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        myViewHolder mvh = new myViewHolder(v, mListener);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder myViewHolder, int i) {
        Entry entry = mEnrties.get(i);
        myViewHolder.mLink.setText(entry.getTitle());
        myViewHolder.mNaam.setText(entry.getLink());
    }

    @Override
    public int getItemCount() {
        return mEnrties.size();
    }
}
