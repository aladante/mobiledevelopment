package com.hva.logic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mInputTT;
    private EditText mInputTF;
    private EditText mInputFF;
    private EditText mInputFT;
    private TextView mScore;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mInputTT = findViewById(R.id.TTinput);
        mInputTF = findViewById(R.id.TFinput);
        mInputFF = findViewById(R.id.FFinput);
        mInputFT = findViewById(R.id.FTinput);
        mButton = findViewById(R.id.checkscore);
        mScore = findViewById(R.id.Score);


        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInput()){
                    mScore.setText("Gewonen");
                } else {
                    mScore.setText("verloren");
                }
            }
        });
    }

    private boolean checkInput(){
        if ("T".equals(mInputTT.getText().toString().toUpperCase())){
            if ("F".equals(mInputTF.getText().toString().toUpperCase())){
                if ("T".equals(mInputFF.getText().toString().toUpperCase())){
                    if ("F".equals(mInputFT.getText().toString().toUpperCase())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
